"use strict";

const l = v => console.log(v);
const HOST = 0;
const GUEST = 1;
const DEFECT = 0;
const GAMEOVER = 3;

//firebase
let db = {};
document.addEventListener("DOMContentLoaded", () => db = firebase.database());

var TargetList = document.getElementById("area");
var DisplayCount = 0;
var USERLEVEL = parseInt(0);

//Vue
const vm = new Vue({
  el: "#main",
  data: {
    roomId: "",
    id: "",
    ref: {},
    sync: {
      host: "",
      guest: "",
      
      turn: -1,//0 host 1 buddy 2 enemy
      judgment: -1,
      board: [],
      
      timestamp: 0,
      count    : 0,
      DefectEnemyCount: 0,
      
      opponentMonster :'',
      opponentSrc     :'',
      opponentHP      :'',
      opponentMax     :'',
      opponentLevel   :'',
      opponentDefense :'',
      
      opponentHpBar: {
       width: "100%"
     },
     opponentFill:100,
     
     
     buddyMonster    :'',
     buddySrc        :'',
     buddyHP         :0,
     buddyMax        :0,
     buddyAttack     :0,
     buddyDefense    :0,
     buddySpeed      :0,
     buddyLevel      :0,

    //Buddy
    buddyHpBar: {
     width: "100%"
   },    
   buddyFill:100,
   
   userMonster     :'',
   userSrc         :'',
   userMax         :0,
   userHP          :0,
   userAttack      :0,
   userDefense     :0,
   userSpeed       :0,
   userLevel       :0,

   userHpBar: {
     width: "100%"
   },
   
   userFill:100,
 },
 
 fightOn: false,
 optionsOn: true,
 
 battleMonster :'',
 battleText: "What will you do?",
 battleOptions: ["Fight", "Monster", "Item", "Escape"],
 userAttackDamage:[0,0,0,0],
 
  //Damege status
  fightOptions: [ "attack","Ember", "Fly", "Frame"],
},

created: function() {this.initGame()},
computed: {
  view: function() {return !this.id ? "lobby" : "game"},
  mark: function() {return this.id == this.sync.host ? 0 : 1},
},
methods: {
    //初期化
    initGame: function() {
      this.sync.turn = -1;
      this.sync.judgment = -1;
      this.sync.count =0;
      this.enemyInit();
      this.sync.DefectEnemyCount = 0;
    },
    enemyInit:function(){
      let src = Math.floor(Math.random() * (49 - 1) + 1);
      let defense = Math.floor((Math.floor(Math.random () * 10) + 5)*(1+this.sync.DefectEnemyCount*0.2));
      console.log("opDef = " + defense);
      
      this.sync.opponentSrc = "../Assets/" + src +".png";
      this.sync.opponentMonster = 'opponent';
      this.sync.opponentLevel = this.sync.DefectEnemyCount+1;
      this.sync.DefectEnemyCount = DisplayCount;
      this.sync.opponentHP =  Math.floor((Math.floor(Math.random () * 10) + 5)*(1+this.sync.DefectEnemyCount*0.5));
      console.log("opHP = " + this.sync.opponentHP );
      this.sync.opponentHpMax =  this.sync.opponentHP;
      this.sync.opponentMax = Math.floor(Math.random() * (200 - 100) + 100);
      this.sync.opponentHpBar.width  ="100%";
      this.sync.opponentFill =100;
      this.sync.opponentDefense = defense;
      this.sync.count ++;
      //DB更新
      this.ref.set(this.sync);
      
    },
    userInit:function(){
     this.sync.userSrc = "../Assets/"  + window.sessionStorage.getItem("imgSrc") + ".png";
     this.sync.userMonster = this.battleMonster = 'HOST';
     this.sync.userHP = this.sync.userMax = window.sessionStorage.getItem("hp")  /*3*/;
     this.sync.userAttack =  window.sessionStorage.getItem("attack");
     this.sync.userDefense = window.sessionStorage.getItem("defense");
     this.sync.userSpeed =   window.sessionStorage.getItem("speed");
     this.sync.userLevel =   window.sessionStorage.getItem("level");
     USERLEVEL = parseInt(this.sync.userLevel);
     this.fightOptions[0] = skillBook["skill" + window.sessionStorage.getItem("skillNumber1")]["name"];
     this.fightOptions[1] = skillBook["skill" + window.sessionStorage.getItem("skillNumber2")]["name"];
     this.fightOptions[2] = skillBook["skill" + window.sessionStorage.getItem("skillNumber3")]["name"];
     this.fightOptions[3] = skillBook["skill" + window.sessionStorage.getItem("skillNumber4")]["name"];
     this.userAttackDamage[0] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber1")]["power"]) + parseInt(this.sync.userAttack);
     this.userAttackDamage[1] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber2")]["power"]) + parseInt(this.sync.userAttack);
     this.userAttackDamage[2] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber3")]["power"]) + parseInt(this.sync.userAttack);
     this.userAttackDamage[3] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber4")]["power"]) + parseInt(this.sync.userAttack);
     console.log("userAttackDamage[0] = " + this.userAttackDamage[0]);
     console.log("userAttackDamage[1] = " + this.userAttackDamage[1]);
     console.log("userAttackDamage[2] = " + this.userAttackDamage[2]);
     console.log("userAttackDamage[3] = " + this.userAttackDamage[3]);
   },
   buddyInit:function(){
    this.sync.buddySrc = "../Assets/"  + window.sessionStorage.getItem("imgSrc") + ".png";
    this.sync.buddyMonster = this.battleMonster = 'GUEST';
    this.sync.buddyLevel = window.sessionStorage.getItem("level");
    this.sync.buddyHP = this.sync.buddyMax = window.sessionStorage.getItem("hp") /* 3*/;
    this.sync.buddyAttack = window.sessionStorage.getItem("attack");
    this.sync.buddyDefense = window.sessionStorage.getItem("defense");
     //this.sync.buddySpeed = window.sessionStorage.getItem("speed");
     this.fightOptions[0] = skillBook["skill" + window.sessionStorage.getItem("skillNumber1")]["name"];
     this.fightOptions[1] = skillBook["skill" + window.sessionStorage.getItem("skillNumber2")]["name"];
     this.fightOptions[2] = skillBook["skill" + window.sessionStorage.getItem("skillNumber3")]["name"];
     this.fightOptions[3] = skillBook["skill" + window.sessionStorage.getItem("skillNumber4")]["name"];
     this.userAttackDamage[0] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber1")]["power"]) + parseInt(this.sync.buddyAttack);
     this.userAttackDamage[1] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber2")]["power"]) + parseInt(this.sync.buddyAttack);
     this.userAttackDamage[2] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber3")]["power"]) + parseInt(this.sync.buddyAttack);
     this.userAttackDamage[3] = parseInt(skillBook["skill" + window.sessionStorage.getItem("skillNumber4")]["power"]) + parseInt(this.sync.buddyAttack);

   },
    //部屋作成
    createRoom: async function() {
      //Guestが来るまで操作出来ないようにturnを-1に
      this.sync.turn = -1;
      this.userInit();    
      //id生成
      this.id = this.createId();
      this.sync.host = this.id;
      this.roomId = this.id.substr(4);
      
      //DB参照
      this.ref = db.ref("/battle/" + this.roomId);
      //対象room情報取得
      const snapshot = await this.ref.once("value");
      //既に部屋があったらリトライ
      if (snapshot.val()) {this.createRoom(); return}
      
      //timestamp
      this.sync.timestamp = moment(new Date).format("YYYY/MM/DD HH:mm:ss");
      //DB更新
      this.ref.set(this.sync);
      //DBイベント定義
      this.setPush();
      
    },
    
    //部屋に入る
    goRoom: async function() {
      //空入力チェック
      if (this.roomId == "") {
       swal("Can't find a room"); 
       return;
     }
     
      //DB参照
      this.ref = db.ref("/battle/" + this.roomId);
      //対象room情報取得
      const snapshot = await this.ref.once("value");
      //部屋あるかチェック
      if (!snapshot.val()) {swal("Can't find a room"); return}
      
      //DB情報取得
      this.sync = snapshot.val();                                                  //key
      //既にguestがいるかチェック
      if (this.sync.guest != "") {swal("This room is no vacancy!"); return}
      
      //guest id生成、hostと被ったら再生成
      let count = 0;
      do {
        this.id = this.createId();
      } while (this.id == this.sync.host && count < 5);
      if (count == 5) {alert("error!"); return}
      
      this.buddyInit();
      //guest更新
      this.sync.guest = this.id;
      //turn値をランダム取得
      this.sync.turn = HOST;
      
      //DB更新
      this.ref.set(this.sync);
      //DBイベント定義
      this.setPush();
      
    },
    
    //DBイベント定義
    setPush: function() {
      this.ref.on("value", function(snapshot) {
        //DBデータをローカルへ反映
        vm.sync = snapshot.val();
        //終了判定
        vm.gemaSet();
      });
      
    },
    
    //置けるかチェック
    checkPut: function() {
      //自分の番じゃなければ処理せず
      if (this.sync.turn != this.mark){ 
       document.getElementById("area").value += "\nNo your turn now";
       goBottom("area");
       swal("No your turn now");
       return false;
     }
     return true;
   },
   
   enemyAttack:function(){
     let target = Math.floor(Math.random()*(100 - 0) + 0) % 2;
     let damage = Math.floor((Math.floor(Math.random () * 10) + 5)*(1+this.sync.DefectEnemyCount*0.2));
     console.log("damage = " + damage);
     
     if(this.sync.userHP <= 0 && this.sync.buddyHP <= 0){
      this.gemaSet();
      console.log("target = " + target);}
    else if(this.sync.userHP <= 0){
      target = GUEST;
    }
    else if (this.sync.buddyHP <= 0){
      target = HOST;
    }
    
    if(this.sync.opponentHP > 0){
    /*HostのHpが0以上*/
     if(target == HOST ){
       //敵の攻撃力-Hostの防御力
       damage -= this.sync.userDefense;
       //ダメージがマイナスになるのを防ぐ
       if(damage < 0) damage = 0;
       //HPをdamage分引く
       this.sync.userHP -= damage;
       setTimeout(() => {
         
       }, 2000);
       
    //テキストエリア編集
    document.getElementById("area").value += "\n" + "[Enemy] " + this.sync.userMonster +" taken " +damage;
    goBottom("area");
    this.battleText = "enemy Attacks " + this.sync.userMonster +" taken " +damage;
    
   //edit if HP !== 0
   this.sync.userFill = (this.sync.userHP / this.sync.userMax) *100;
    //Edit Hp bar
    if (this.sync.userFill <= 0) {
     this.sync.userHpBar.width = "0%";
     this.sync.userHP = 0;
   }else{
     this.sync.userHpBar.width = this.sync.userFill + "%";
   }
 }
 
/*BuddyHpが0以上*/
else if(target == GUEST){
 damage -= this.sync.userDefense;
 if(damage < 0)
   damage = 0;
 this.sync.buddyHP -=damage;
 setTimeout(() => {

 }, 2000);
 document.getElementById("area").value += "\n" + "[Enemy] " + this.sync.buddyMonster +" taken " +damage;
 goBottom("area");
 this.battleText = "enemy Attacks!!! " + this.sync.buddyMonster +" taken " +damage;

    //edit if HP !== 0
    this.sync.buddyFill = (this.sync.buddyHP / this.sync.buddyMax) * 100;
   //Edit Hp bar
   if (this.sync.buddyFill <= 0) {
    this.sync.buddyHpBar.width = "0%";
    this.sync.buddyHP = 0;
  }else{
    this.sync.buddyHpBar.width = this.sync.buddyFill + "%";
  }
}
}

this.gemaSet();
  //ターン交代
  if(this.sync.turn == HOST && this.sync.buddyHP  > 0){
    this.sync.turn = GUEST;
  }else if (this.sync.turn == GUEST && this.sync.userHP > 0){
    this.sync.turn = HOST;
  }
  
    //DB更新
    this.ref.set(this.sync);
    
  },
  
    //勝敗判定
    judge: function() {
      //勝敗判定
      if(this.sync.opponentHP <= 0){
       return DEFECT;
     }else if(this.sync.buddyHP <=0 && this.sync.userHP <= 0){
       return GAMEOVER;
     }
      //押す場所がなくなった判定
      
      //上記以外
      return -1;
    },
    
    //command
    processOption:function(option){
     
      if(this.checkPut() == true){
        
        switch(option){
         case 1:
         this.optionsOn = false;
         this.fightOn = true;
         break;
         case 2:
   //handle pokemon
   setTimeout(() => {
    this.battleText = "What will " + this.battleMonster + " do?";
  }, 2000);
   this.battleText = "You're our only hope " + this.battleMonster + "!";
   break;
   
   case 3:
   //handle item
   setTimeout(() => {
    this.battleText = "What will " + this.battleMonster + " do?";
  }, 2000);
   this.battleText = "No items in bag.";
   break;
   
   case 4:
   setTimeout(() => {
    this.battleText = "What will " + this.battleMonster + " do?";
  }, 2000);
   this.battleText = "Can't escape.";
   break;
 }
}
},
    //終了判定
    gemaSet: function() {
      
      this.sync.judgment = this.judge();
      //既定値なら何もせず
      
      if  (this.sync.judgment == -1) return;
      
      //盤面操作無効化
      //chrome用にwait
      setTimeout(function(){
        //勝敗出力
        switch(vm.sync.judgment) {
         //敵を倒した
          case DEFECT: 
           document.getElementById("area").value += "\n[Player] You win!";//swal("You win!");
           goBottom("area");
           vm.enemyInit();
           //this.sync.DefectEnemyCount++;
           DisplayCount ++;//= this.sync.DefectEnemyCount;
           break;
           
          case 1:
           break;
           
          case 2:
           break;
          
          case GAMEOVER:
           console.log("DisplayCount = " + DisplayCount);
           /*終了ボタンの表示*/
           var options = {
             title: "Game Over...",
             text: "You defeated" + DisplayCount +" enemies",
             type: "warning",
             buttons: {
              ok: true
            }
          };
          var levelText = {
           title : "Level Up",
           text : "You get " + Math.floor(DisplayCount/2) + " SP",
           type: 'success'
          };
          /* OKボタンが押されたら遷移 */
          swal(options).then(function(val) {
           /* Level Up */
              if(Math.floor(DisplayCount/2) > 0){
               swal(levelText).then(function(val) {
               USERLEVEL += parseInt(Math.floor(DisplayCount/2));
               window.sessionStorage.setItem("sp",Math.floor(DisplayCount/2)); 
               window.sessionStorage.setItem("level",USERLEVEL);
               puripuri_submit("../Level_Up/level_up.html");
               });
              }
              else{
               puripuri_submit("../Menu/menu.html");
              }
          });
          break;
          
         default:
          break;
        }
        
        //host側で初期化
        if (vm.mark == 0 && vm.sync.userHP > 0 && vm.syncbuddyHP > 0) {
          //初期化
          vm.initGame();
          //DBも初期化
          vm.ref.set(vm.sync);
        }
      }, 10);
    },
    
    processAttack: function(attack){
  //add comment
  switch(attack){
    case 1:
    setTimeout(() => {
    }, 2000);
    document.getElementById("area").value += "\n[Player] " + this.battleMonster + " plays " + this.fightOptions[0];
    goBottom("area");
    this.battleText = this.battleMonster + "plays " + this.fightOptions[0];
  //handle scratch
  break;
  case 2:
  setTimeout(() => {
  }, 2000);
  
  document.getElementById("area").value += "\n[Player] " + this.battleMonster + " plays " +this.fightOptions[1]; 
  goBottom("area");
  this.battleText = this.battleMonster + "plays " +this.fightOptions[1]; 
  //handle scratch
  break;
  case 3:
  setTimeout(() => {
  }, 2000);
  
  document.getElementById("area").value += "\n[Player] " + this.battleMonster + "plays " +this.fightOptions[2]; 
  goBottom("area");
  this.battleText = this.battleMonster + "plays " +this.fightOptions[2]; 
  //handle scratch
  break;
  
  case 4:
  setTimeout(() => {
  }, 2000);
  
  document.getElementById("area").value += "\n[Player] " + this.battleMonster + "plays " +this.fightOptions[3]; 
  goBottom("area");
  this.battleText = this.battleMonster + "plays " +this.fightOptions[3]; 
  //handle scratch
  break;
}

document.getElementById("area").value += "\n[Player] " + (this.userAttackDamage[attack-1]-this.sync.opponentDefense) + " damege is given" ; 
goBottom("area");
this.sync.opponentHP -= (this.userAttackDamage[attack-1]-this.sync.opponentDefense);
  //edit if HP !== 0
  this.sync.opponentFill = (this.sync.opponentHP / this.sync.opponentHpMax) * 100;
  //Edit Hp bar
  if (this.sync.opponentFill <= 0) {
   this.sync.opponentHpBar.width = "0%";
 }else{
   this.sync.opponentHpBar.width = this.sync.opponentFill + "%";
 }
 
 setTimeout(() => {
   this.battleText = "What will " + this.sync.userMonster + " do?";
 }, 2000);
 
 this.optionsOn = true;
 this.fightOn = false;
 this.enemyAttack();
 this.gemaSet();
 this.gemaSet();

    //DB更新
    this.ref.set(this.sync);
    
  },
    //id生成
    createId: () => String(Math.random()).substr(2,8),
    
    //配列、数値をOXに変換
    parseOX: val => {
      if (val == 0) return "Host";
      if (val == 1) return "Guest";
    },
    
  }
})
/* 画面遷移(Html切り替え)の関数 */
function puripuri_submit(action) {
 document.form_puripuri.action = action;
 document.form_puripuri.submit();
}

/* テキストエリアの最下へ移動するための関数 */
function goBottom(targetId) {
  var obj = document.getElementById(targetId);
  if(!obj) return;
  obj.scrollTop = obj.scrollHeight;
}