var skillBook = {
    /* 0~19 : ノーマルタイプ */
    /*   初期スキル 0~9   */
    
    //たいあたり
    "skill0": {
        "name": "Tackle",
        "type": "Physics",
        "power": 2,
        "accuracy": 100,
        "pp": 35
    },
    
    //とっしん
    "skill1": {
        "name": "Take Down",
        "type": "Physics",
        "power": 3,
        "accuracy": 85,
        "pp": 20 
    },
    
    //なきごえ
    "skill2": {
        "name": "Growl",
        "type": "EnemyAttack",
        "power": 2,
        "accuracy": 100,
        "pp": 40
    },
    
    //にらみつける
    "skill3": {
        "name": "Leer",
        "type": "EnemyDefense",
        "power": 2,
        "accuracy": 100,
        "pp": 30
    },
    
    //はたく
    "skill4": {
        "name": "Pound",
        "type": "Physics",
        "power": 4,
        "accuracy": 80,
        "pp": 45
    },
    
    //はねる
    "skill5": {
        "name": "Splash",
        "type": "UserAttack",
        "power": 5,
        "accuracy": 5,
        "pp": 40
    },
    
    //高速移動
    "skill6": {
        "name": "Agility",
        "type": "UserDefense",
        "power": 4,
        "accuracy": 60,
        "pp": 20
    },
    
    //ばくれつパンチ
    "skill7": {
        "name": "D-Punch",
        "type": "Physics",
        "power": 7,
        "accuracy": 50,
        "pp": 10
    },
    
    //ひっかく
    "skill8": {
        "name": "Scratch",
        "type": "Physics",
        "power": 3,
        "accuracy": 100,
        "pp": 15
    },
    
    //ふみつけ
    "skill9": {
        "name": "Stomp",
        "type": "Physics",
        "power": 4,
        "accuracy": 60,
        "pp": 30
    },
    
    //
    
    
    /* 20~39 : ほのおタイプ */
    /*   初期スキル 20~29   */
    
    //ひのこ
    "skill10": {
        "name": "Ember",
        "type": "Frame",
        "power": 2,
        "accuracy": 100,
        "pp": 35
    },
    
    //かえんほうしゃ
    "skill11": {
        "name": "Flame",
        "type": "Frame",
        "power": 5,
        "accuracy": 100,
        "pp": 10
    },
    
    //かえんぐるま
    "skill12": {
        "name": "Flame Wheel",
        "type": "Frame",
        "power": 3,
        "accuracy": 90,
        "pp": 40
    },
    
    //おにび
    "skill13": {
        "name": "Will-O-Wisp",
        "type": "Frame",
        "power": 4,
        "accuracy": 80,
        "pp": 45
    },
    
    //だいもんじ
    "skill14": {
        "name": "Fire Blast",
        "type": "Frame",
        "power": 8,
        "accuracy": 40,
        "pp": 20
    },
    
    //エアカッター
    "skill15": {
        "name": "Air Cutter",
        "type": "NULL",
        "power": 5,
        "accuracy": 0,
        "pp": 0
    },
    
    //エアースラッシュ
    "skill16": {
        "name": "Air Slash",
        "type": "NULL",
        "power": 4,
        "accuracy": 0,
        "pp": 0
    },
    
    //アクアジェット
    "skill17": {
        "name": "Aqua Jet",
        "type": "NULL",
        "power": 7,
        "accuracy": 0,
        "pp": 0
    },
    
    //アクアリング
    "skill18": {
        "name": "Aqua Ring",
        "type": "NULL",
        "power": 5,
        "accuracy": 0,
        "pp": 0
    },
    
    //波動弾
    "skill19": {
        "name": "Aura Sphere",
        "type": "NULL",
        "power": 8,
        "accuracy": 0,
        "pp": 0
    },
    
    //しめつける
    "skill20": {
        "name": "Bind",
        "type": "NULL",
        "power": 3,
        "accuracy": 0,
        "pp": 0
    },
    
    //ブラストバーン
    "skill21": {
        "name": "Blast Burn",
        "type": "NULL",
        "power": 6,
        "accuracy": 0,
        "pp": 0
    },
    
    //ブレイズキック
    "skill22": {
        "name": "Blaze Kick",
        "type": "NULL",
        "power": 4,
        "accuracy": 0,
        "pp": 0
    },
    
    //ブリザード
    "skill23": {
        "name": "Blizzard",
        "type": "NULL",
        "power": 4,
        "accuracy": 0,
        "pp": 0
    },
    
    //のしかかり
    "skill24": {
        "name": "Body Slam",
        "type": "NULL",
        "power": 3,
        "accuracy": 0,
        "pp": 0
    },
    
    //バブル光線
    "skill25": {
        "name": "BubbleBeam",
        "type": "NULL",
        "power": 3,
        "accuracy": 0,
        "pp": 0
    },
    
    //タネマシンガン
    "skill26": {
        "name": "Bullet Seed",
        "type": "NULL",
        "power": 4,
        "accuracy": 100,
        "pp": 0
    },
    
    //悪の波動
    "skill27": {
        "name": "Dark Pulse",
        "type": "NULL",
        "power": 7,
        "accuracy": 0,
        "pp": 0
    },
    
    //放電
    "skill28": {
        "name": "Discharge",
        "type": "NULL",
        "power": 2,
        "accuracy": 0,
        "pp": 0
    },
    
    //エナジーボール
    "skill29": {
        "name": "Energy Ball",
        "type": "NULL",
        "power": 5,
        "accuracy": 0,
        "pp": 0
    },
    
    //噴火
    "skill30": {
        "name": "Eruption",
        "type": "NULL",
        "power": 7,
        "accuracy": 0,
        "pp": 0
    },
    
    //きあいだま
    "skill31": {
        "name": "Focus Blast",
        "type": "NULL",
        "power": 4,
        "accuracy": 0,
        "pp": 0
    },
    
    //ずつき
    "skill32": {
        "name": "Headbutt",
        "type": "NULL",
        "power": 2,
        "accuracy": 0,
        "pp": 0
    },
    
    //ねっぷう
    "skill33": {
        "name": "Heat Wave",
        "type": "NULL",
        "power": 3,
        "accuracy": 0,
        "pp": 0
    },
    
    //つのどりる
    "skill34": {
        "name": "Horn Drill",
        "type": "NULL",
        "power": 6,
        "accuracy": 0,
        "pp": 0
    },
    
    //ハイドロポンプ
    "skill35": {
        "name": "Hydro Pump",
        "type": "NULL",
        "power": 8,
        "accuracy": 0,
        "pp": 0
    },
    
    
}