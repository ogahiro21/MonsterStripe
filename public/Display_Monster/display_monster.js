var monster = []; 
var skill;
var monster_name = window.sessionStorage.getItem("imgSrc");

window.onload = function () {
  String(monster_name);
  console.log("Level = " + window.sessionStorage.getItem("level"));
  //loadScript("../skillBook.js", skill);
  //console.log("skillName : "+skill.skillBook.skill0.name);
  var app = new Vue({
    el: '#app',
    data: {
      barcode : window.sessionStorage.getItem("barcode"),
      userHp : window.sessionStorage.getItem("hp"),
      userAttack : window.sessionStorage.getItem("attack"),
      userDefense : window.sessionStorage.getItem("defense"),
      userSpeed : window.sessionStorage.getItem("speed"),
      userMonsterSrc : "../Assets/"+ monster_name + ".png",
      
      PlayerSkillNum1 : window.sessionStorage.getItem("skillNumber1"),
      PlayerSkillNum2 : window.sessionStorage.getItem("skillNumber2"),
      PlayerSkillNum3 : window.sessionStorage.getItem("skillNumber3"),
      PlayerSkillNum4 : window.sessionStorage.getItem("skillNumber4"),
      
      PlayerSkillName1 : skillBook["skill" + window.sessionStorage.getItem("skillNumber1")]["name"],
      PlayerSkillName2 : skillBook["skill" + window.sessionStorage.getItem("skillNumber2")]["name"],
      PlayerSkillName3 : skillBook["skill" + window.sessionStorage.getItem("skillNumber3")]["name"],
      PlayerSkillName4 : skillBook["skill" + window.sessionStorage.getItem("skillNumber4")]["name"],
      
      PlayerSkillPp1 : skillBook["skill" + window.sessionStorage.getItem("skillNumber1")]["pp"],
      PlayerSkillPp2 : skillBook["skill" + window.sessionStorage.getItem("skillNumber2")]["pp"],
      PlayerSkillPp3 : skillBook["skill" + window.sessionStorage.getItem("skillNumber3")]["pp"],
      PlayerSkillPp4 : skillBook["skill" + window.sessionStorage.getItem("skillNumber4")]["pp"],
      
    },
    
  })
  displayPieChart();
}

function puripuri_submit(action) {
document.form_puripuri.action = action;
document.form_puripuri.submit();
}

/*  Lader Chartの導入 */

function displayPieChart() {
  var data = {
    labels: ["HP \n"+window.sessionStorage.getItem("hp"), "Attack "+window.sessionStorage.getItem("attack"), "Defense "+window.sessionStorage.getItem("defense"), "Speed "+window.sessionStorage.getItem("speed")],
    datasets: [
      {
        fillColor: "rgba(42,22,130,0.5)",
        borderColor : "rgba(42,22,130,0.8)",
        strokeColor: "rgba(42,22,130,0.8)",
        pointColor: "rgba(42,22,130,1)",
        pointHighlightFill: "purple",
        pointHighlightStroke: "purple",
        data: [window.sessionStorage.getItem("hp"), window.sessionStorage.getItem("attack"),window.sessionStorage.getItem("defense"), window.sessionStorage.getItem("speed")]
      }
    ]
  };
  var ctx = document.getElementById("radarChart").getContext("2d");
  var options = { 
    scaleOverride : true,
    scaleSteps : 1,
    scaleStepWidth : 100,
    scaleStartValue : 0,
    //スケールラインの色 
    scaleLineColor : "rgb(42, 22, 130)",//ライン色
    scaleFontSize : 500,//サイズ 
    scaleShowLabels : false,
  };
  var radarChart = new Chart(ctx).Radar(data, options);
}