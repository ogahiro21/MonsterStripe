
//Vue
window.onload = function () {
    var app = new Vue({
        el: "#app",
        data: {
            //barcode : 1,//window.sessionStorage.getItem("barcode"),
            userHp : window.sessionStorage.getItem("hp"),
            userAttack : window.sessionStorage.getItem("attack"),
            userDefense : window.sessionStorage.getItem("defense"),
            userSpeed : window.sessionStorage.getItem("speed"),
            statusPoint : window.sessionStorage.getItem("sp"),
            spMax : 0,
            hpmMax : 0,
            attackMax : 0,
            defenseMax : 0,
            speedMax: 0,
            btnUpFlag : false,
            btnDownFlag : true,
        },        
        created: function() {this.init()},

        methods : {
            
            init : function(){
                this.spMax = this.statusPoint;
                this.hpmMax = this.userHp;
                this.attackMax = this.userAttack;
                this.defenseMax = this.userDefense;
                this.speedMax = this.userSpeed;
            },
            
            statusUp : function(option){
                this.statusPoint--;
                this.btnDownFlag = false;
                switch(option){
                    case 1:
                        this.userHp++;
                        break;
                        
                    case 2:
                        this.userAttack++;
                        break;
                        
                    case 3:
                        this.userDefense++;
                        break;
                    
                    case 4:
                        this.userSpeed++;
                        break;
                }
                
                if(this.statusPoint <= 0) {
                    this.btnUpFlag = true;
                }
            },
            
            statusDown : function(option){
                switch(option){
                    case 1:
                        if(this.userHp > this.hpmMax){
                        this.userHp--;
                        HP = this.userHp;
                        this.pointCalc();
                        }break;
                        
                    case 2:
                        if(this.userAttack > this.attackMax){
                        this.userAttack--;
                        this.pointCalc();
                        }
                        break;
                        
                    case 3:
                        if(this.userDefense > this.defenseMax){
                        this.userDefense--;
                        this.pointCalc();
                        }
                        break;
                    
                    case 4:
                        if(this.userSpeed > this.speedMax){
                        this.userSpeed--;
                        this.pointCalc();
                        }
                        break;
                
                    if(this.statusPoint >= this.spMax) {
                        this.btnDownFlag = true;
                    }
                    
                }
            },
            
            pointCalc : function(){
                this.statusPoint++;
                this.btnUpFlag = false;
            },
            
            /* 確認ボタン表示 */
            enter : function(){
                let HP = this.userHp;
                let ATTACK = this.userAttack;
                let DEFENSE = this.userDefense;
                let SPEED = this.userSpeed;
                
                let option = {
                    title: "Final check",
                    text: "Enter the status?",
                    type: "warning",
                    button: {
                        ok: true,
                        cancel: "Cancel"
                    }
                };
                swal(option).then(function(val) {
                    /* ステータスを更新 */
                    console.log("this = " + this.userHp);
                    console.log("HP = " + HP);
                    window.sessionStorage.setItem("hp",HP);
                    window.sessionStorage.setItem("attack",ATTACK);
                    window.sessionStorage.setItem("defense",DEFENSE);
                    window.sessionStorage.setItem("speed",SPEED);
                    puripuri_submit("../Menu/menu.html");
                });
            },
        },
        
    });
}

/* 画面遷移(Html切り替え)の関数 */
function puripuri_submit(action) {
 document.form_puripuri.action = action;
 document.form_puripuri.submit();
}
