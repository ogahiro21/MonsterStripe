var barcode = [];
var monster = []; 
var cheatCode = [7,7,7,7];
var char_number = 49;
var cheetCount = 0;
const SKILLNUM = 36;

/**/
function rand_init () {
  var sheed;
  
  if(barcode[0] <= 9 || barcode[0] >= 0)　sheed = barcode[0];
  if(barcode[0] == cheatCode[0]) cheetCount++;
  else sheed = 1;

  for(var i=1; i<barcode.length; i++){
    if(barcode[i-1] <= 9 && barcode[i-1] >= 0){
    if(barcode[i] <= 9 && barcode[i] >= 0) sheed += barcode[i]*barcode[i-1];
    if(barcode[i] == cheatCode[i]) cheetCount++;
    }
    else sheed+=1;
  }

  Math.seed = sheed;  

  Math.seededRandom = function(max, min) {
    max = max || 1;
    min = min || 0;

    Math.seed = (Math.seed * 9301 + 49297) % 233280;
    var rnd = Math.seed / 233280;

    return min + rnd * (max - min);
  }
}

window.onload = function init (){
  swal({
    title: 'Scan Barcode',
    input: 'text',
    inputPlaceholder: 'Input your barcode',
    //showCancelButton: true,
    inputValidator: function (value) {
      return new Promise(function (resolve, reject) {
        if (value) {
          resolve();
        } else {
          reject('You need to scan barcode!');
        }
      })
    }
  }).then(function (name) {
    window.sessionStorage.setItem("barcode",name);
    barcode = window.sessionStorage.getItem("barcode");
    main();

  })
}

function MakeMomster(){

    //Status
    monster.hp = Math.seededRandom(20,5);
    monster.attack = Math.seededRandom(20,5);
    monster.defense = Math.seededRandom(20,5);
    monster.speed = Math.seededRandom(20,5);
    monster.imgSrc = Math.seededRandom(char_number,1);
    
    //Status floor
    monster.hp = Math.floor(monster.hp);
    monster.attack = Math.floor(monster.attack);
    monster.defense = Math.floor(monster.defense);
    monster.speed = Math.floor(monster.speed);
    monster.imgSrc = Math.floor(monster.imgSrc);
    
    //Skill
    monster.skillNumber1 = Math.seededRandom(SKILLNUM,0);
    monster.skillNumber1 = Math.floor(monster.skillNumber1);
    while(1){
      monster.skillNumber2 = Math.seededRandom(SKILLNUM,0);
      monster.skillNumber2 = Math.floor(monster.skillNumber2);
      if(monster.skillNumber1 != monster.skillNumber2) break;
    }
    while(1){
      monster.skillNumber3 = Math.seededRandom(SKILLNUM,0);
      monster.skillNumber3 = Math.floor(monster.skillNumber3);
      if(monster.skillNumber1 != monster.skillNumber3 && monster.skillNumber2 != monster.skillNumber3) break;
    }
    while(1){
      monster.skillNumber4 = Math.seededRandom(SKILLNUM,0);
      monster.skillNumber4 = Math.floor(monster.skillNumber4);
      if(monster.skillNumber1 != monster.skillNumber4 && monster.skillNumber2 != monster.skillNumber4 && monster.skillNumber3 != monster.skillNumber4) break;
    }
    
    monster.level = 1;

    //Set window.sessionStorage
    window.sessionStorage.setItem("hp",monster.hp);
    window.sessionStorage.setItem("attack",monster.attack);
    window.sessionStorage.setItem("defense",monster.defense);
    window.sessionStorage.setItem("speed",monster.speed);
    window.sessionStorage.setItem("imgSrc",monster.imgSrc);
    window.sessionStorage.skillNumber1 = monster.skillNumber1;
    window.sessionStorage.setItem("skillNumber2",monster.skillNumber2);
    window.sessionStorage.setItem("skillNumber3",monster.skillNumber3);
    window.sessionStorage.setItem("skillNumber4",monster.skillNumber4);
    window.sessionStorage.setItem("userLevel",monster.level);
    window.sessionStorage.setItem("level",1);
    window.sessionStorage.setItem("sp",0);
    
    if(cheetCount == 4){
    window.sessionStorage.setItem("hp",100);
    window.sessionStorage.setItem("attack",100);
    window.sessionStorage.setItem("defense",100);
    window.sessionStorage.setItem("speed",100);
    window.sessionStorage.setItem("imgSrc",4/*34*/);
    window.sessionStorage.setItem("skill1Number",monster.skillNumber1);
    window.sessionStorage.setItem("skill2Number",monster.skillNumber2);
    window.sessionStorage.setItem("skill3Number",monster.skillNumber3);
    window.sessionStorage.setItem("skill4Number",monster.skillNumber4);
    }
      
    }

function puripuri_submit(action) {
    document.form_puripuri.action = action;
    document.form_puripuri.submit();
}

function main() {
    rand_init();
    MakeMomster();
    
  /*---Buddy---*/
    //make_buddy();
  /*----------*/
  
      
  /*---Egg Spin---*/
    eggSpin();
  /*--------------*/
  }


  
/*--------------------*/
function eggSpin(){
  enchant();
  var flag = 1;
  
  /* コアを作る */
  var core = new Core(500,500);

  core.preload('../Assets/egg.png', "../Assets/nestFront.png", "../Assets/nestBack.png", "../Assets/Flare.png");
  
  /* 画面切り替えの速さ */
  core.fps = 10;
  
  /* 読み込んだら表示 */
  core.onload = function(){
    var frameCount = 0;
    var clickFlag = 1;

    
    /* タマゴの読み込み */
    var egg = new Sprite(500,750);
    egg.image = core.assets['../Assets/egg.png'];
    egg.x = 0;
    egg.y = -65;
    egg.scale(0.3,0.3);
    
    /* 巣(前)の読み込み */
    var nestFront = new Sprite(600,250);
    nestFront.image = core.assets['../Assets/nestFront.png'];
    nestFront.x = -50;
    nestFront.y = 230;
    nestFront.scale(0.5,0.5);
    
    /* 巣(後ろ)の読み込み */
    var nestBack = new Sprite(600,250);
    nestBack.image = core.assets['../Assets/nestBack.png'];
    nestBack.x = -40;
    nestBack.y = 120;
    nestBack.scale(0.5,0.5);
    
    /* 後光の読み込み */
    var flare = new Sprite(500,500);
    flare.image = core.assets['../Assets/Flare.png'];
    flare.x = 0;
    flare.y = 10;
    flare.scale(0.001,0.001);

    
    /* タマゴをうごかす */ 
    /* enterframe -> 新しいフレームになったら */ 
    egg.addEventListener('enterframe' , function () {
      frameCount++;
     if(frameCount == 30) puripuri_submit("../Menu/menu.html");
     
     else{
      if(egg.rotation > 7) flag = true;
      if(flag == true) this.rotate(-2);
      if(egg.rotation < -7) flag = false;
      if(flag == false) this.rotate(2);
      if(frameCount > 10) {
        flare.rotate(10);
        flare.scale(1.5,1.5);
      }
     }

    });
    
      /* 画像の表示 */
    core.rootScene.addChild(nestBack);
    core.rootScene.addChild(egg);
    core.rootScene.addChild(nestFront);
    core.rootScene.addChild(flare);

    core.push
  } 
  core.start();
  
};
